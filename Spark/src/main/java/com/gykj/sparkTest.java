package com.gykj;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaSparkContext$;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @Description（描述）: java版本的spark @Version（版本）: 1.0 @Author（创建者）: ALIENWARE @Date（创建时间）: Created in
 * 2021/7/3. @ * * * * * * * * * * * * * @
 */
public class sparkTest {

  public static void main(String[] args) {
      SparkConf sparkConf = new SparkConf().setAppName("wordcountforjava").setMaster("local[*]");
      // 创建java版本的sparkCioontext对象
      JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);
      // rdd里面的元素是 word world word
      JavaRDD<String> rdd = javaSparkContext.textFile("xxxxx");
      JavaRDD<String> flatMapRdd =
          rdd.flatMap(new FlatMapFunction<String, String>() {

                @Override
                public Iterator<String> call(String line) throws Exception {
                  String[] arr = line.split("\t");
                  return Arrays.asList(arr).iterator();
                }
              });

      // "word" --> ("word", 1)
      JavaRDD<Tuple2<String, Integer>> pairRdd =
          flatMapRdd.map(
              new Function<String, Tuple2<String, Integer>>() {
                public Tuple2<String, Integer> call(String v1) throws Exception {
                  return new Tuple2<String, Integer>(v1, 1);
                }
              });

      // ("word", 1) , 按照单词进行聚合
      JavaPairRDD<String, Iterable<Tuple2<String, Integer>>> groupByRdd =
          pairRdd.groupBy(
              new Function<Tuple2<String, Integer>, String>() {
                @Override
                public String call(Tuple2<String, Integer> v1) throws Exception {
                  return v1._1;
                }
              });

      JavaPairRDD<String, Integer> mapValuesRdd =
          groupByRdd.mapValues(
              new Function<Iterable<Tuple2<String, Integer>>, Integer>() {
                public Integer call(Iterable<Tuple2<String, Integer>> it) throws Exception {
                  int sum = 0;
                  for (Tuple2<String, Integer> t : it) {
                    sum += t._2;
                  }
                  return sum;
                }
              });

      // collect是action算子，当执行collect，会把executor端mapValuesRdd的数据全部拉取到driver端
      // 因为是把executor端数据全部拉取回来，如果driver端的内存不够，那就造成内存溢出了
      // 注意使用collect时，要知道executor端拉取的数据量情况
      List<Tuple2<String, Integer>> list = mapValuesRdd.collect();
      for (Tuple2<String, Integer> t : list) {
        System.out.println(t);
      }

      // take是拉取回来两条
      //        List<Tuple2<String, Integer>> list = mapValuesRdd.take(2);
      //        for(Tuple2<String, Integer> t : list){
      //            System.out.println(t);
      //        }
  }
}
