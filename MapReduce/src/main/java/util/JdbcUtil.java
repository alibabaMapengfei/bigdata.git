package util;

import com.alibaba.druid.pool.DruidDataSource;

public class JdbcUtil {
	
	
	// 创建数据库的连接对象
	private static final String CONN_DRIVER = "com.mysql.jdbc.Driver";
	private static final String CONN_URL = "jdbc:mysql://127.0.0.1:3306/counter_db?characterEncoding=UTF-8";
	private static final String CONN_USER = "root";
	private static final String CONN_PASSWORD = "root";
	
	
	// 创建数据源对象
	private static DruidDataSource dataSource = new DruidDataSource();
	
	// 赋值
	static{
		dataSource.setDriverClassName(CONN_DRIVER);
		dataSource.setUrl(CONN_URL);
		dataSource.setUsername(CONN_USER);
		dataSource.setPassword(CONN_PASSWORD);
	}

	public static DruidDataSource getDataSource() {
		return dataSource;
	}
	
	

	
	

	
}
