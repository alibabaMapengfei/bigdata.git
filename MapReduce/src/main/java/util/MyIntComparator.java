package util;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * key 实现自定义比较器的时候
 * @author Su
 *
 */
public class MyIntComparator extends WritableComparator {
	
	// 1. 告诉人家你需要比较什么类型的数据  true:是否创建实例
	public MyIntComparator() {
		super(IntWritable.class, true);
	}

	// 2. 实现比较方法
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		//compareTo b>a 输出1  b<a输出-1  b=a 输出0
		return b.compareTo(a);
	}
}
