package org.prac.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 多reduce分数排序输出
 */
// 1	盖谦	2001-01-14	11480630	1	东城区第1中学	1	东城区	540
public class StudentWritable1 implements WritableComparable<StudentWritable> {
	
	// 定义学生属性
	private String sname = "";			// 学生姓名
	private String birthday = "";		// 出生日期
	private String examno = "";			// 准考证号
	private String scname = "";			// 学校名称
	private String aname = "";			// 地区名称
	private Integer score = 0;			// 考生分数
	
	
	
	

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getExamno() {
		return examno;
	}

	public void setExamno(String examno) {
		this.examno = examno;
	}

	public String getScname() {
		return scname;
	}

	public void setScname(String scname) {
		this.scname = scname;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}
	
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return sname + "\t" + birthday + "\t" + examno + "\t"
				+ scname + "\t" + aname + "\t" + score;
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		
		out.writeUTF(this.sname);
		out.writeUTF(this.birthday);
		out.writeUTF(this.examno);
		out.writeUTF(this.scname);
		out.writeUTF(this.aname);
		out.writeInt(this.score);
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
		this.sname = in.readUTF();
		this.birthday = in.readUTF();
		this.examno = in.readUTF();
		this.scname = in.readUTF();
		this.aname = in.readUTF();
		this.score = in.readInt();
		
	}

	@Override
	public int compareTo(StudentWritable o) {
		return o.getScore().compareTo(this.getScore());
	}
	
	
	

}
