package org.prac.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 左链接序列化
 */
public class AreaWritable implements WritableComparable<AreaWritable> {
	
	
	// 定义属性
	private Integer aid = -1;		// 地区ID
	private String aname = "";		// 地区名称
	private Integer year = -1;		// 从业年份
	private Long count = -1L;		// 从业人数
	
	public Integer getAid() {
		return aid;
	}

	public void setAid(Integer aid) {
		this.aid = aid;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
	
	
	

	@Override
	public String toString() {
		return aid + "\t" + aname + "\t" + year + "\t" + count;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		
		out.writeInt(this.aid);
		out.writeUTF(this.aname);
		out.writeInt(this.year);
		out.writeLong(this.count);
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
		this.aid = in.readInt();
		this.aname = in.readUTF();
		this.year = in.readInt();
		this.count = in.readLong();
		
		
	}

	@Override
	public int compareTo(AreaWritable o) {
		return this.getAid().compareTo(o.getAid());
	}

}
