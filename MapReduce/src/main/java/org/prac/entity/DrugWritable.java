package org.prac.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 药品序列化实现
 * 最大值最小值
 */
public class DrugWritable implements WritableComparable<DrugWritable> {
	
	
	private String name = "";
	private Double pay = 0D;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPay() {
		return pay;
	}
	public void setPay(Double pay) {
		this.pay = pay;
	}
	@Override
	public String toString() {
		return name + "\t" + pay;
	}
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeUTF(this.name);
		out.writeDouble(this.pay);
		
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		this.name = in.readUTF();
		this.pay = in.readDouble();
		
	}
	@Override
	public int compareTo(DrugWritable o) {
		return this.getName().compareTo(o.getName());
	}
	
	
	
	

}
