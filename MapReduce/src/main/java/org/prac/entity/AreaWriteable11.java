package org.prac.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 二次排序
 */
public class AreaWriteable11 implements WritableComparable<AreaWriteable11> {
	
	// 定义属性
	private String userName = "";
	private Long userCost = 0L;
	
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserCost() {
		return userCost;
	}

	public void setUserCost(Long userCost) {
		this.userCost = userCost;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return userName + "\t" + userCost;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		
		
		out.writeUTF(this.userName);;
		out.writeLong(this.userCost);
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
		this.userName = in.readUTF();
		this.userCost = in.readLong();
		
	}

	@Override
	public int compareTo(AreaWriteable11 o) {
		
		// 先比较第一个字段
		int result = o.getUserName().compareTo(this.getUserName());
		if(result == 0){
			result = o.getUserCost().compareTo(this.getUserCost());
		}
		return result;
	}
	

}
