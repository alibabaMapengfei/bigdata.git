package org.prac.entity;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @desc 自定义序列化方法
 *  hadoop自定义序列化 与 反序列化
 * @Author ALIENWARE
 * @Date 2021/2/24 18:07
 * @Version 1.0
 */

public class StudentWritable implements WritableComparable<StudentWritable> {

	// 定义属性
	private String examNo = "";
	private String studentName = "";
	private String birthday = "";
	private String school = "";
	private String areaName = "";
	private Integer score = -1;




	public String getExamNo() {
		return examNo;
	}

	public void setExamNo(String examNo) {
		this.examNo = examNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public String toString()
	{
		return "StudentWritable{" +
				"examNo='" + examNo + '\'' +
				", studentName='" + studentName + '\'' +
				", birthday='" + birthday + '\'' +
				", school='" + school + '\'' +
				", areaName='" + areaName + '\'' +
				", score=" + score +
				'}';
	}

	@Override
	public int compareTo(StudentWritable o)
	{
		return o.getScore().compareTo(this.getScore());
	}

	//序列化方法
	@Override
	public void write(DataOutput out) throws IOException
	{
		out.writeUTF(this.examNo);
		out.writeUTF(this.studentName);
		out.writeUTF(this.birthday);
		out.writeUTF(this.school);
		out.writeUTF(this.areaName);
		out.writeInt(this.score);

	}

	//反序列化方法
	@Override
	public void readFields(DataInput in) throws IOException
	{
		this.examNo = in.readUTF();
		this.studentName = in.readUTF();
		this.birthday = in.readUTF();
		this.school = in.readUTF();
		this.areaName = in.readUTF();
		this.score = in.readInt();
	}
}
