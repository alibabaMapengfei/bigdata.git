package org.prac.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class Area1Writable implements WritableComparable<Area1Writable> {
	
	private Integer aid = -1;
	private String aname = "";
	
	
	
	public Integer getAid() {
		return aid;
	}

	public void setAid(Integer aid) {
		this.aid = aid;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(this.aid);
		out.writeUTF(this.aname);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.aid = in.readInt();
		this.aname = in.readUTF();
		
	}
	
	
	
	
	@Override
	public String toString() {
		return aid + "\t" + aname;
	}

	// 设置属性比较
	@Override
	public int compareTo(Area1Writable o) {
		return o.getAid().compareTo(this.getAid());
	}

	
	
}
