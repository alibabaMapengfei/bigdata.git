package org.prac.task.bin;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.prac.task.mr.Distinct;
import org.prac.task.mr.Max;
import org.prac.task.mr.WordCount;
import org.prac.task.utils.BaseMR;
import org.prac.task.utils.JobRunnerUtil;
import org.prac.task.utils.JobRunnerUtilResult;


/**
 * 整体任务的运行
 * @author mpf
 *
 */
public class MyRunner extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		
		// 获取配置文件的加载对象
		Configuration conf = this.getConf();
		// BaseMR conf设置
		BaseMR.setConf(conf);
		// 创建任务工作链的管理者
		JobControl ctrl = new JobControl("mpf_group1");
		
		// 创建job
		ControlledJob worcount = new WordCount().getControlledJob();
		ControlledJob max = new Max().getControlledJob();
		ControlledJob distinct = new Distinct().getControlledJob();
		
		// 进行JOB的设置
		max.addDependingJob(worcount);
		distinct.addDependingJob(worcount);
		
		// 添加job
		ctrl.addJob(worcount);
		ctrl.addJob(distinct);
		ctrl.addJob(max);
		
		// 运行 任务工作链
		JobRunnerUtilResult result = JobRunnerUtil.run(ctrl);
		
		if(result.getIsOK()){
			
			Map<String,Counters> map = result.getCountersMap();
			
			for (Entry<String, Counters> entry : map.entrySet()) {
				
				System.out.println(entry.getKey() + "'s counters count : " + entry.getValue().countCounters());
				
				for (CounterGroup cg : entry.getValue()) {
					System.out.println("\t" + cg.getDisplayName());
					for (Counter counter : cg) {
						System.out.println("\t\t" + counter.getDisplayName() + "=" + counter.getValue());
					}
				}
				
				
			}
			
			System.out.println("JOB_STATUS : OK!");
			System.out.println("JOB_COST : " + result.getRunTime());
		}
		
		
		return 0;
	}
	
	public static void main(String[] args) {
		try {
			System.exit(ToolRunner.run(new MyRunner(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
