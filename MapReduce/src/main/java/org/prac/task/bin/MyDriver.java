package org.prac.task.bin;

import org.apache.hadoop.util.ProgramDriver;

public class MyDriver {

	public static void main(String[] args) {
		
		ProgramDriver driver = new ProgramDriver();
		try {
			driver.addClass("p1", MyRunner.class, "进行任务工作链测试");
			// 运行短命令
			ProgramDriver.class.getMethod("driver", new Class[]{String[].class}).invoke(driver, new Object[]{args});
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
