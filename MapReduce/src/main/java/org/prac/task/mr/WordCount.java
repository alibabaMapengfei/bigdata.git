package org.prac.task.mr;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.prac.task.utils.BaseMR;
import org.prac.task.utils.MyConstant;


public class WordCount extends BaseMR {
	
	private static class MyMapper extends Mapper<LongWritable, Text, Text, LongWritable>{
		
		// 定义map需要用到的变量
		private Text outkey = new Text();
		private LongWritable outval = new LongWritable(1);
		private String[] strs = null;
		
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, LongWritable>.Context context)
				throws IOException, InterruptedException {
			
			// 切分数据
			strs = value.toString().split(MyConstant.SPLIT_STR1);
			// 循环
			for (String s : strs) {
				outkey.set(s);
				context.write(outkey, outval);
			}
		}
		
		
	}
	
	private static class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable>{
		
		// 创建reduce需要用到的变量
		private LongWritable outval = new LongWritable();
		private Long sum = 0L;
		
		@Override
		protected void reduce(Text outkey, Iterable<LongWritable> values,
				Reducer<Text, LongWritable, Text, LongWritable>.Context context) throws IOException, InterruptedException {
			
			// 初始化
			sum = 0L;
			// 循环
			for (LongWritable l : values) {
				sum+=l.get();
			}
			// 设置
			outval.set(sum);
			// 输出
			context.write(outkey, outval);
		}
		
		
	}
	
	

	@Override
	public Job getJob(Configuration conf) throws Exception {
		
		// 创建方法的返回值
		Job job = Job.getInstance(conf, getJobNameWithTaskID());
		
		// 设置类
		job.setJarByClass(WordCount.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setCombinerClass(MyReducer.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		
		// 设置路径
		FileInputFormat.addInputPath(job, new Path(conf.get(MyConstant.TASK_INPUT)));
		FileOutputFormat.setOutputPath(job, this.getOutPutPath());
		
		// 返回
		return job;
	}

	@Override
	public String getJobName() {
		return "wordcount";
	}

}
