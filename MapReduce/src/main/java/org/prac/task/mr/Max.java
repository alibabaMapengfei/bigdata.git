package org.prac.task.mr;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.prac.task.utils.BaseMR;
import org.prac.task.utils.MyConstant;

/**
 * max拿到的是wordcount的输出
 */
public class Max extends BaseMR {

	private static class MyMapper extends Mapper<LongWritable, Text, Text, Text>{
		
		// 创建map函数需要用到的变量
		private Text outkey = new Text("x");
		private Text outval = new Text();
		private String[] strs = null;
		
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {
			// 切分数据
			strs = value.toString().split(MyConstant.SPLIT_STR1);
			
			// 完成数据拼接
			outval.set(strs[0].trim() + MyConstant.SPLIT_STR2 + strs[1].trim());
			
			// 输出
			context.write(outkey, outval);
			
		}
		
		
	}
	
	private static class MyReducer extends Reducer<Text, Text, Text, LongWritable>{
		
		// 创建reduce需要用到的变量
		private Text outkey = new Text();
		private LongWritable outval = new LongWritable();
		
		
		// 创建冒泡变量
		private String current_key = "";
		private Long current_val = 0L;
		private String max_key = "";
		private Long max_val = 0L;
		
		@Override
		protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, LongWritable>.Context context)
				throws IOException, InterruptedException {
			
			// 进行数据的循环
			for (Text t : values) {
				current_key = t.toString().split(MyConstant.SPLIT_STR2)[0].trim();
				current_val = Long.parseLong(t.toString().split(MyConstant.SPLIT_STR2)[1].trim());
				
				if(current_val >= max_val){
					max_key = current_key;
					max_val = current_val;
				}
				
			} 
			
			outkey.set(max_key);
			outval.set(max_val);
			context.write(outkey, outval);
		}
		
		
	}
	
	

	@Override
	public Job getJob(Configuration conf) throws Exception {
		
		// 创建方法的返回值
		Job job = Job.getInstance(conf, getJobNameWithTaskID());
		
		// 设置类
		job.setJarByClass(Max.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		
		// 设置路径
		FileInputFormat.addInputPath(job, new WordCount().getOutPutPath());
		FileOutputFormat.setOutputPath(job, this.getOutPutPath());
		
		// 返回
		return job;
	}

	@Override
	public String getJobName() {
		return "max";
	}
	
	
}
