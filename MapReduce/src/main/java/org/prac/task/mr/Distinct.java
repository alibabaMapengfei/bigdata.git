package org.prac.task.mr;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.prac.task.utils.BaseMR;
import org.prac.task.utils.MyConstant;

/**
 * wordcount的输入作为他的输出
 */
public class Distinct extends BaseMR {
	
	private static class MyMapper extends Mapper<LongWritable, Text, Text, NullWritable>{
		
		// 创建map需要用到的变量
		private Text outkey = new Text();
		private String[] strs = null;
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// 拆分数据
			strs = value.toString().split(MyConstant.SPLIT_STR1);
			outkey.set(strs[0].trim());
			// 输出
			context.write(outkey, NullWritable.get());
		}
		
		
	}
	
	
	private static class MyReducer extends Reducer<Text, LongWritable, Text, NullWritable>{
		
		@Override
		protected void reduce(Text outkey, Iterable<LongWritable> values,
				Reducer<Text, LongWritable, Text, NullWritable>.Context context) throws IOException, InterruptedException {
			
			context.write(outkey, NullWritable.get());
			
			
		}
		
		
	}
	
	

	@Override
	public Job getJob(Configuration conf) throws Exception {
		
		// 创建方法的返回值
		Job job = Job.getInstance(conf, getJobNameWithTaskID());
		
		// 设置类
		job.setJarByClass(Distinct.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setCombinerClass(MyReducer.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		
		// 设置路径
		FileInputFormat.addInputPath(job, new WordCount().getOutPutPath());
		FileOutputFormat.setOutputPath(job, this.getOutPutPath());
		
		// 返回
		return job;
	}

	@Override
	public String getJobName() {
		return "distinct";
	}
	
	
}
