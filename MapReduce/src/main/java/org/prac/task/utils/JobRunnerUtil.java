package org.prac.task.utils;

import java.util.concurrent.*;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;


public class JobRunnerUtil {

	/**
	 * 自定义线程名称,方便的出错的时候溯源
	 */
	private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("test-pool-%d").build();

	/**
	 * corePoolSize    线程池核心池的大小
	 * maximumPoolSize 线程池中允许的最大线程数量
	 * keepAliveTime   当线程数大于核心时，此为终止前多余的空闲线程等待新任务的最长时间
	 * unit            keepAliveTime 的时间单位
	 * workQueue       用来储存等待执行任务的队列
	 * threadFactory   创建线程的工厂类
	 * handler         拒绝策略类,当线程池数量达到上线并且workQueue队列长度达到上限时就需要对到来的任务做拒绝处理
	 */
	private static ExecutorService service = new ThreadPoolExecutor(
			1,
			1,
			0L,
			TimeUnit.MILLISECONDS,
			new LinkedBlockingQueue<>(1024),
			namedThreadFactory,
			new ThreadPoolExecutor.AbortPolicy()
	);

	// 创建一个线程池 创建一个长度为1的定长线程池 负责监控线程的运行
	//private static ExecutorService ex = Executors.newFixedThreadPool(1);
	

	private static class JobRunnerCallable implements Callable<JobRunnerUtilResult>{
		
		
		private JobControl ctrl;
		
		public JobRunnerCallable(JobControl ctrl) {
			this.ctrl = ctrl;
		}




		@Override
		public JobRunnerUtilResult call() throws Exception {
			
			// 运行监控
			
			// 设定返回值
			JobRunnerUtilResult result = new JobRunnerUtilResult();
			
			long start = System.currentTimeMillis();
			// 阻塞当前线程保证任务工作链执行完成
			while(!ctrl.allFinished()){
				// 还有任务没执行
				Thread.sleep(2000);
			}
			long end = System.currentTimeMillis();
			
			// 任务工作链就执行成功了
			// 判断任务是否执行成功
			if(ctrl.getFailedJobList().size() == 0){
				result.setIsOK(true);
			}else{
				result.setIsOK(false);
			}
			// 计算运行时间
			result.setRunTime(this.getLifeTime(Math.abs(start - end)));
			
			// 进行counter的整理
			for (ControlledJob cjob : ctrl.getSuccessfulJobList()) {
				// counters
				result.getCountersMap().put(cjob.getJob().getJobName(), cjob.getJob().getCounters());
			}
			
			// 停止任务工作链运行
			ctrl.stop();
			
			// 返回
			return result;
		}
		
		
		private String getLifeTime(Long mss){
            // 计算用了多少天
            Long days = mss / (1000*60*60*24);
            // 计算用了多少个小时 取余数算出不足一天的毫秒数 然后除以小时的毫秒 转换成小时单位
            Long hours = (mss%(1000*60*60*24))/(1000*60*60);
            // 计算用了多少分钟
            Long minutes = (mss%(1000*60*60))/(1000*60);
            // 计算用了所少秒钟
            Long seconds = (mss%(1000*60))/1000;
            // 开始拼接时间
            StringBuilder sb = new StringBuilder();
            // 判断
            if(days != 0){
                sb.append(days).append("天");
            }
            if(hours != 0){
                sb.append(hours).append("小时");
            }
            if(minutes != 0){
                sb.append(minutes).append("分钟");
            }
            if(seconds != 0){
                sb.append(seconds).append("秒");
            }
            return sb.toString();
        }
		
	}
	
	
	
	public static JobRunnerUtilResult run(JobControl ctrl)throws Exception {
		
		// 执行 ctrl
		Thread t = new Thread(ctrl);
		t.start();
		// 线程池运行监控线程
		java.util.concurrent.Future<JobRunnerUtilResult> result =  service.submit(new JobRunnerCallable(ctrl));
		return result.get();
	}

}
