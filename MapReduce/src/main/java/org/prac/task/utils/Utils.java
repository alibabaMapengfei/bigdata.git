package org.prac.task.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * 对象是否为null的判断工具类
 * @author mpf
 *
 */
public class Utils {
	
	/**
	 * 判断对象是否为null 如果为null 返回true
	 * @param obj
	 * @return
	 */
	public static Boolean isEmpty(Object obj){
		
		// 判断一下是不是对象
		if(null == obj){
			return true;
		}else if(obj instanceof String){ // 字符串是否为空
			return "".equals(String.valueOf(obj).trim());
		}else if(obj instanceof Collection<?>){ // List Set
			return ((Collection<?>)obj).isEmpty();
		}else if(obj instanceof Map<?,?>){ // hashMap
			return ((Map<?,?>)obj).isEmpty();
		}else if(obj.getClass().isArray()){
			return Array.getLength(obj) == 0;
		}
		// 默认非空
		return false;
	}
	
	/**
	 * 判断对象是否非空
	 * @param obj
	 * @return
	 */
	public static Boolean isNotEmpty(Object obj){
		return !isEmpty(obj);
	}
	
	

}
