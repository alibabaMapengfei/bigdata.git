package org.prac.mapreduce;

import java.io.IOException;

import org.prac.entity.Area1Writable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import util.MyIntComparator;


public class MyMRDemo10 extends Configured implements Tool {

	
	private static class MyMapper extends Mapper<LongWritable, Text, Area1Writable, NullWritable>{
		
		private Area1Writable outkey = new Area1Writable();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Area1Writable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			strs = value.toString().split("\t");
			outkey.setAid(Integer.parseInt(strs[0].trim()));
			outkey.setAname(strs[1].trim());
			context.write(outkey, NullWritable.get());
			
		}
		
	}
	
	private static class MyReducer extends Reducer<Area1Writable, NullWritable,  Area1Writable, NullWritable>{
		
		private Area1Writable outkey = new Area1Writable();
		
		@Override
		protected void reduce(Area1Writable key, Iterable<NullWritable> values,
				Reducer<Area1Writable, NullWritable, Area1Writable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			outkey.setAid(key.getAid());
			outkey.setAname(key.getAname());
			context.write(outkey, NullWritable.get());
		}
	}
	
	
	
	@Override
	public int run(String[] args) throws Exception {
		
		// 获取配置文件
		Configuration conf = this.getConf();
		// 创建job
		Job job = Job.getInstance(conf, "desc sort");
		
		
		// 类设置
		job.setJarByClass(MyMRDemo10.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(Area1Writable.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setSortComparatorClass(MyIntComparator.class);
		
		// 第二步 设置路径
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			// hadoop fs -rm -r -skipTrash --> 递归删除
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行
		long start = System.currentTimeMillis();
		boolean con = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
		System.out.println(msg);
		System.out.println("JOB_COST : " + ((end - start) / 1000) + " SECONDS!");
		
		return 0;
	}
	
	public static void main(String[] args) {
		
		try {
			System.exit(ToolRunner.run(new MyMRDemo10(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
