package org.prac.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * mapreduce优化1
 * @desc counter的使用 -->统计符合需求的数据
 * @Author ALIENWARE
 * @Date 2021/2/23 20:25
 * @Version 1.0
 */
public class MyWordCount02 extends Configured implements Tool {


	private static final String SPRLIT_STR = "\t";//分隔符


	protected static class MyMapper extends Mapper<LongWritable,Text,Text,LongWritable> {

		//定义map需要用到的变量
		private Text outkey = new Text();
		private LongWritable outval = new LongWritable(1);
		private String[] strs = null; //map的数据

		@Override
		public void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, LongWritable>.Context context)
				throws IOException, InterruptedException
		{
			try{
			//拆分数据
			// map 进行数的拆分
			strs = value.toString().split(SPRLIT_STR);
			// map使用ounter
			context.getCounter("Line Quality Statistics", "Total Line Count").increment(1);
			// 业务判断 是好 还是 坏
			if(null != strs && strs.length == 3){
				// 好
				context.getCounter("Line Quality Statistics", "Nice Line Count").increment(1);
				outkey.set(strs[2].trim());
				// 输出
				context.write(outkey, outval);
			}else{
				// 坏
				context.getCounter("Line Quality Statistics", "Bad Line Count").increment(1);
			}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	protected static class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable>{

		// 创建reduce函数需要用到的变量
		private LongWritable outval = new LongWritable();
		private long sum = 0L;


		@Override
		protected void reduce(Text outkey, Iterable<LongWritable> values,
							  Reducer<Text, LongWritable, Text, LongWritable>.Context context) throws IOException, InterruptedException {

			// 数据累加
			sum = 0;
			// 遍历累加
			for (LongWritable l : values) {
				sum+=l.get();
			}
			// 赋值
			outval.set(sum);
			// 输出
			context.write(outkey, outval);
		}
	}

	@Override
	public int run(String[] args)
	{
		try {
			//获取已经加载好的配置的conf
			Configuration conf = this.getConf();
			//编写本次job
			Job job = Job.getInstance(conf);
			//job开始进行  固定三部配置

			//1. 类的配置 主执行类设置,谁有main方法就设置谁
			job.setJarByClass(MyWordCount02.class);
			//设置数据的输入格式化类
			job.setInputFormatClass(TextInputFormat.class);
			job.setMapperClass(MyMapper.class);//设置map
			//如果map和 reduce的输出来星一样.可以用一个输出
			//job.setMapOutputKeyClass(Text.class);//map  key的输出  固定的
			//job.setOutputValueClass(LongWritable.class);//map的 value输出 固定的
			job.setReducerClass(MyReducer.class);//设置reduce
			job.setOutputKeyClass(Text.class);//reduce的key
			job.setOutputValueClass(LongWritable.class);//reduce的vcalue
			job.setOutputFormatClass(TextOutputFormat.class);//设置输出
			//2. 路径设置
			//输入路径
			//FileInputFormat.addInputPath(job,new Path(args[0]));
			//保证输出路径必须没有
			Path in = new Path(args[0]);
			Path out = new Path(args[1]);
			FileSystem fs = FileSystem.get(conf);
			if (fs.exists(out)) {
				fs.delete(out, true);
				System.out.println(job.getJobName() + "路径已经被删除了!");
			}
			FileInputFormat.addInputPath(job, in);
			FileOutputFormat.setOutputPath(job, out);
			// 3.执行配置
			long start = System.currentTimeMillis();
			//
			boolean cons = job.waitForCompletion(true);
			long end = System.currentTimeMillis();
			String msg = "job状态" + (cons ? "SUCCESS!" : "FAILE!");

			// 没有log4j的情况记录counter 展示counter
			if (cons) {
				//map task reduce task --> ci=ounter --> 执行完成之后 applicationmaster 统计
				Counters counters = job.getCounters();
				System.out.println(job.getJobName() + "'s counters count : " + counters.countCounters());
				for (CounterGroup counter : counters) {
					System.out.println("\t"+ counter.getDisplayName());
					for (Counter counter1 : counter) {
						System.out.println("\t\t"+counter1.getDisplayName() + "=" + counter1.getValue());
					}
				}
			}

			System.out.println(msg);
			System.out.println(Math.abs(end-start)/1000+"秒!");

		}catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}


	public static void main(String[] args)
	{
		try {
			System.exit(ToolRunner.run(new MyWordCount02(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
