package org.prac.mapreduce;

import org.prac.entity.StudentWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;


/**
 * 返回四个partitioner,给4个reduce
 * partionar应用
 * 开启4个reduce , map给到partionner分区,然后交给多个reduce处理
 */

public class MapPartitioner04 extends Configured implements Tool {

	//定义数据分隔符
	private static final String SPLIT_STR1 = "\t";

	//map
	//inputformat -->(一行数据的起始 和一行数据的值) 0 aabbc--> a 1 a 1 b 1 b 1 c 1
	private static class MyMapper extends Mapper<LongWritable, Text, StudentWritable, NullWritable>{

		// 定义map需要用到的环境变量
		private StudentWritable outkey = new StudentWritable();
		private String[] strs = null;


		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, StudentWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {

			// map 进行数的拆分
			strs = value.toString().split(SPLIT_STR1);
			// 演示counter
			context.getCounter("Line Quality Statistics", "Total Line Count").increment(1);
			// 1	盖谦	2001-01-14	11480630	1	东城区第1中学	1	东城区	540
			// 业务判断 是好 还是 坏
			if(null != strs && strs.length == 9){
				// 好
				context.getCounter("Line Quality Statistics", "Nice Line Count").increment(1);
				outkey.setExamNo(strs[4].trim());
				outkey.setStudentName(strs[1].trim());
				outkey.setBirthday(strs[2].trim());
				outkey.setSchool(strs[5].trim());
				outkey.setAreaName(strs[7].trim());
				outkey.setScore(Integer.parseInt(strs[8].trim()));
				context.write(outkey, NullWritable.get());
			}else{
				// 坏
				context.getCounter("Line Quality Statistics", "Bad Line Count").increment(1);
			}


		}

	}

	//partitoner
	private static class MyPartitioner extends Partitioner<StudentWritable, NullWritable> {

		// 通过设置 getPartition 进行reduceID的获取
		@Override
		public int getPartition(StudentWritable key, NullWritable value, int numPartitions) {

			// 一本 550 分以上 包含550分
			if(key.getScore() >= 550){
				return 0;
			}
			if(key.getScore() >=450 && key.getScore() < 550){
				return 1;
			}
			if(key.getScore() >= 250 && key.getScore() < 450){
				return 2;
			}
			return 3;
		}

	}

	//mapreduce主要执行的任务

	@Override
	public int run(String[] args)
	{
		try {
		//获取已经加载好的配置的conf
		Configuration conf = this.getConf();
		//编写本次job
		Job job = Job.getInstance(conf);
		//job开始进行  固定三部配置

		//1. 类的配置 主执行类设置,谁有main方法就设置谁
		job.setJarByClass(MapPartitioner04.class);
		//设置数据的输入格式化类
		job.setInputFormatClass(TextInputFormat.class);
		job.setPartitionerClass(MyPartitioner.class);
		job.setNumReduceTasks(4);
		job.setMapperClass(MyMapper.class);//设置map
		job.setOutputKeyClass(StudentWritable.class);//reduce的key
		job.setOutputValueClass(NullWritable.class);//reduce的vcalue
		job.setOutputFormatClass(TextOutputFormat.class);//设置输出
		//2. 路径设置
		//输入路径
		//FileInputFormat.addInputPath(job,new Path(args[0]));
		//保证输出路径必须没有
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if(fs.exists(out)){
			fs.delete(out,true );
			System.out.println(job.getJobName() + "路径已经被删除了!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job,out);
		// 3.执行配置
		long start = System.currentTimeMillis();
		//
		boolean cons = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "job状态" + (cons? "SUCCESS!":"FAILE!");
		System.out.println(msg);
		System.out.println(Math.abs(end-start)/1000+"秒!");

		}catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}



	//运行mapreduce
	/**
	 * mapreduce运行流程
	 * 1.ToolRunner.run 获取tool.getConf() tool接口的configretion
	 * 2.extends Configured 获取Configuration 对象,加载hadoop配置文件
	 * 3.ToolRunner.run接管mapreduce执行,进行参数设置
	 */
	public static void main(String[] args)
	{
		try {
			System.exit(ToolRunner.run(new MapPartitioner04(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
//设置reduce输出压缩设置 (3种方式)
// -Dmapreduce.output.fileoutputformat.compress=true
// -Dmapreduce.output.fileoutputformat.compress.codec=org.apache.hadoop.io.compress.GzipCodec 输入路径/文件 输出路径
/*设置一个参数有三种方法
		1. -D参数传递数据 针对的是针对本次运行的job 适中
		2. conf.set(key,val) 针对具体MapReduce 影响范围最小
		3. xml 参数传递
他们的参数加载顺序是这样的
	xml 最先被加载
	-D会重写xml的同名属性
	conf.set会重写-D的同名属性*/
