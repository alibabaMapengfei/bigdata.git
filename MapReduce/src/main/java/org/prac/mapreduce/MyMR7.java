package org.prac.mapreduce;

import java.io.IOException;

import org.prac.entity.AreaWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


/**
 * 内连接查询
 * 利用多目录输入设置,读取到两个map的信息,通过分隔符,利用reduce切割数据,
 * 然后输出对应的结果
 */
public class MyMR7 extends Configured implements Tool {

	private static final String SPLIT_STR1 = "\t";
	private static final String SPLIT_STR2 = "\001";
	
	
	// 处理地区
	private static class MyMapper1 extends Mapper<LongWritable, Text, IntWritable, Text> {
		
		// 创建map需要用到的变量
		private IntWritable outkey = new IntWritable();
		private Text outval = new Text();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			
			// 数据整理
			strs = value.toString().split(SPLIT_STR1); // [1,北京]
			
			// 防御式编程
			if(null != strs && strs.length == 2){
				
				outkey.set(Integer.parseInt(strs[0].trim()));
				outval.set("a" + SPLIT_STR2 + strs[1].trim());
				// 输出
				context.write(outkey, outval);
			}
			
		}
		
	}
	
	private static class MyMapper2 extends Mapper<LongWritable, Text, IntWritable, Text> {
		
		
		// 创建map需要用到的变量
		private IntWritable outkey = new IntWritable();
		private Text outval = new Text();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			
			// 数据整理
			strs = value.toString().split(SPLIT_STR1); // [1,2010,1900]
			
			// 防御式编程
			if(null != strs && strs.length == 3){
				
				outkey.set(Integer.parseInt(strs[0].trim()));
				outval.set("b" + SPLIT_STR2 + strs[1].trim() + SPLIT_STR1  + strs[2].trim());
				// 输出
				context.write(outkey, outval);
			}
			
		}
		
		
	}
	

	private static class MyReducer extends Reducer<IntWritable, Text, AreaWritable, NullWritable> {

		// 创建reduce需要用到的变量
		private AreaWritable outkey = new AreaWritable();
		private String tmp = "";
		private String aname = "";
		private String[] strs = null;
		@Override
		protected void reduce(IntWritable key, Iterable<Text> values,
				Reducer<IntWritable, Text, AreaWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			
			// 初始化变量
			tmp = "";
			
			// 循环变量
			for (Text t : values) {
				tmp+=t.toString() + ",";
			}
			
			// 1. 判断需要的数据 同时包含a 和 b的数据
			if(tmp.indexOf("a") > -1 && tmp.indexOf("b") > -1){
				// 获取地区名称
				aname = tmp.substring(tmp.indexOf("a"));
				aname = aname.substring(2,aname.indexOf(","));
				strs = tmp.substring(tmp.indexOf("b")).split(",");
				// 循环
				for (String s : strs) {
					
					if(s.startsWith("b")){ // b\0012010\t1900
						
						// 年份和就业数据
						outkey.setAid(key.get());
						outkey.setAname(aname);
						outkey.setYear(Integer.parseInt(s.split(SPLIT_STR2)[1].trim().split(SPLIT_STR1)[0].trim()));
						outkey.setCount(Long.parseLong(s.split(SPLIT_STR2)[1].trim().split(SPLIT_STR1)[1].trim()));
						context.write(outkey, NullWritable.get());
					}
				}
			}
		}
	}

	@Override
	public int run(String[] args) throws Exception {

		// 创建本次的job
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "innerjoin");

		// 设置 job

		// 第一步设置类
		job.setJarByClass(MyMRDemo7.class);
		job.setReducerClass(MyReducer.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(AreaWritable.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		// 第二步 设置路径
		Path out = new Path(args[args.length-1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			// hadoop fs -rm -r -skipTrash --> 递归删除
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		
		// 多目录输入的设置
		// 用于处理 地区信息
		MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MyMapper1.class);
		// 用于处理从业人数
		MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MyMapper2.class);
		
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行
		long start = System.currentTimeMillis();
		boolean con = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
		System.out.println(msg);
		System.out.println("JOB_COST : " + ((end - start) / 1000) + " SECONDS!");

		return 0;
	}

	public static void main(String[] args) {

		try {
			System.exit(ToolRunner.run(new MyMRDemo7(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
