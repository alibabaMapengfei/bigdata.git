package org.prac.mapreduce;

import java.io.IOException;
import java.text.SimpleDateFormat;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *  mapreduce 最大值最小值实现
 */

public class MapReduceManMin06 extends Configured implements Tool {

	private static final String SPLIT_STR1 = "\t";
	private static final String SPLIT_STR2 = "\001";

	private static class MyMapper extends Mapper<LongWritable, Text, Text, Text> {

		// 定义map需要用到的环境变量 //这个x就是map传给reduce的key值
		private Text outkey = new Text("x");
		private Text outval = new Text();
		private String[] strs = null;

		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {

			// map 进行数的拆分
			strs = value.toString().split(SPLIT_STR1);

			// 2018-01-01 001616528 236701 强力VC银翘片 6.0 82.8 69.0
			context.getCounter("Line Quality Statistics", "Total Line Count").increment(1);
			if (null != strs && strs.length == 7) {
				context.getCounter("Line Quality Statistics", "Nice Line Count").increment(1);
				// 3 和 6
				outval.set(strs[3].trim() + SPLIT_STR2 + strs[6].trim());
				context.write(outkey, outval);

			} else {
				context.getCounter("Line Quality Statistics", "Bad Line Count").increment(1);
			}
		}

	}

	private static class MyCombiner extends Reducer<Text, Text, Text, Text> {

		private Text outval = new Text();

		// 冒泡排序
		private String current_key = "";
		private Double current_val = 0D;
		private String max_key = "";
		private Double max_val = 0D;
		private String min_key = "";
		private Double min_val = 0D;

		@Override
		protected void reduce(Text outkey, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {

			// 遍历数据
			for (Text t : values) {

				current_key = t.toString().split(SPLIT_STR2)[0].trim();
				current_val = Double.parseDouble(t.toString().split(SPLIT_STR2)[1].trim());

				// 冒泡
				if (current_val >= max_val) {
					max_val = current_val;
					max_key = current_key;
				}

				if (current_val <= min_val || min_val == 0) {
					min_val = current_val;
					min_key = current_key;
				}
			}

			outval.set(max_key + SPLIT_STR2 + max_val);
			context.write(outkey, outval);
			outval.set(min_key + SPLIT_STR2 + min_val);
			context.write(outkey, outval);

		}

	}

	private static class MyReducer extends Reducer<Text, Text, Text, DoubleWritable> {

		private Text outkey = new Text();
		private DoubleWritable outval = new DoubleWritable();
		// 冒泡排序
		private String current_key = "";
		private Double current_val = 0D;
		private String max_key = "";
		private Double max_val = 0D;
		private String min_key = "";
		private Double min_val = 0D;

		@Override
		protected void reduce(Text key, Iterable<Text> values,
							  Reducer<Text, Text, Text, DoubleWritable>.Context context) throws IOException, InterruptedException {

			// 遍历数据
			for (Text t : values) {

				current_key = t.toString().split(SPLIT_STR2)[0].trim();
				current_val = Double.parseDouble(t.toString().split(SPLIT_STR2)[1].trim());

				// 冒泡
				if (current_val >= max_val) {
					max_val = current_val;
					max_key = current_key;
				}

				if (current_val <= min_val || min_val == 0) {
					min_val = current_val;
					min_key = current_key;
				}
			}

			outkey.set(max_key);
			outval.set(max_val);
			context.write(outkey, outval);
			//每个context.write会输出一个数据
			outkey.set(min_key);
			outval.set(min_val);
			context.write(outkey, outval);
		}

	}

	@Override
	public int run(String[] args) throws Exception {

		// 创建本次的job
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "distinct");

		// 设置 job

		// 第一步设置类
		job.setJarByClass(MapReduceManMin06.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setCombinerClass(MyCombiner.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		// 第二步 设置路径
		Path in = new Path(args[0]);
		Path in1 = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行
		long start = System.currentTimeMillis();
		boolean con = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
		System.out.println(msg);
		return 0;
	}

	public static void main(String[] args) {

		try {
			System.exit(ToolRunner.run(new MapReduceManMin06(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
//使用idea编写
//edit configruation 添加路径
//E:\MAPREDUCEFILE\FILE\INPUT\file1 E:\MAPREDUCEFILE\FILE\OUTPUT