package org.prac.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * mapreduce 去重实现 ,驱虫的数据放到mapreduce的key上,value输出nullwritable空值
 */

public class MapReduceDistinct05 extends Configured implements Tool {

	//定义数据分隔符
	private static final String SPRLIT_STR = "\t";

	private static class MyMapper extends Mapper<LongWritable, Text, Text, NullWritable>{

		// 定义map需要用到的环境变量
		private Text outkey = new Text();
		private String[] strs = null;
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, NullWritable>.Context context)
				throws IOException, InterruptedException {

			// map 进行数的拆分
			strs = value.toString().split(SPRLIT_STR);

			// 遍历
			for (String s : strs) {
				outkey.set(s);
				context.write(outkey, NullWritable.get());
			}
		}
	}

	//reduce阶段
	public static class MyReducer extends Reducer<Text, NullWritable, Text, NullWritable>
	{
		//定义reduce需要用到的环境变量
		private LongWritable outval = new LongWritable();
		private Long sum = 0L;//map传过来的累加的数据


		@Override
		public void reduce(Text outkey, Iterable<NullWritable> values,
							  Reducer<Text, NullWritable, Text, NullWritable>.Context context) throws IOException, InterruptedException {

			context.write(outkey, NullWritable.get());
		}
	}


	//mapreduce主要执行的任务
	@Override
	public int run(String[] args)
	{
		try {
		//获取已经加载好的配置的conf
		Configuration conf = this.getConf();
		//编写本次job
		Job job = Job.getInstance(conf,"distinct");
		//job开始进行  固定三部配置

		//1. 类的配置 主执行类设置,谁有main方法就设置谁
		job.setJarByClass(MapReduceDistinct05.class);
		//设置数据的输入格式化类
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);//设置map
		job.setReducerClass(MyReducer.class);//设置reduce
		job.setOutputKeyClass(Text.class);//reduce的key
		job.setOutputValueClass(NullWritable.class);//reduce的vcalue
		job.setOutputFormatClass(TextOutputFormat.class);//设置输出
		//2. 路径设置
		//输入路径
		//FileInputFormat.addInputPath(job,new Path(args[0]));
		//保证输出路径必须没有
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if(fs.exists(out)){
			fs.delete(out,true );
			System.out.println(job.getJobName() + "路径已经被删除了!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job,out);
		// 3.执行配置
		long start = System.currentTimeMillis();
		//
		boolean cons = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "job状态" + (cons? "SUCCESS!":"FAILE!");
		System.out.println(msg);
		System.out.println(Math.abs(end-start)/1000+"秒!");

		}catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}



	//运行mapreduce
	/**
	 * mapreduce运行流程
	 * 1.ToolRunner.run 获取tool.getConf() tool接口的configretion
	 * 2.extends Configured 获取Configuration 对象,加载hadoop配置文件
	 * 3.ToolRunner.run接管mapreduce执行,进行参数设置
	 */
	public static void main(String[] args)
	{
		try {
			System.exit(ToolRunner.run(new MapReduceDistinct05(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
//使用idea编写
//edit configruation 添加路径
//E:\MAPREDUCEFILE\FILE\INPUT\file1 E:\MAPREDUCEFILE\FILE\OUTPUT