package org.prac.mapreduce;

import java.io.IOException;


import org.prac.entity.DrugWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * 序列化方式实现最大值最小值
 * 多目录输出reduce结果
 */

public class MyMRDemo7 extends Configured implements Tool {

	private static final String SPLIT_STR1 = "\t";

	private static class MyMapper extends Mapper<LongWritable, Text, DrugWritable, NullWritable> {

		// 定义map需要用到的环境变量
		private DrugWritable outkey = new DrugWritable();
		private String[] strs = null;

		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, DrugWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {

			// map 进行数的拆分
			strs = value.toString().split(SPLIT_STR1);

			// 2018-01-01 001616528 236701 强力VC银翘片 6.0 82.8 69.0
			context.getCounter("Line Quality Statistics", "Total Line Count").increment(1);
			if (null != strs && strs.length == 7) {
				context.getCounter("Line Quality Statistics", "Nice Line Count").increment(1);
				// 3 和 6
				outkey.setName(strs[3].trim());
				outkey.setPay(Double.parseDouble(strs[6].trim()));
				context.write(outkey, NullWritable.get());

			} else {
				context.getCounter("Line Quality Statistics", "Bad Line Count").increment(1);
			}
		}

	}
//combiner
private static class MyCombiner extends Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable> {

	private DrugWritable outkey = new DrugWritable();




	// 冒泡排序
	private String current_key = "";
	private Double current_val = 0D;
	private String max_key = "";
	private Double max_val = 0D;
	private String min_key = "";
	private Double min_val = 0D;

	@Override
	protected void reduce(DrugWritable key, Iterable<NullWritable> values, Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable>.Context context)
			throws IOException, InterruptedException {

		current_key = key.getName();
		current_val = key.getPay();

		if(current_val >= max_val){
			max_key = current_key;
			max_val = current_val;
		}
		if(current_val <= min_val || min_val == 0){
			min_val = current_val;
			min_key = current_key;
		}
	}

	@Override
	protected void cleanup(Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable>.Context context)
			throws IOException, InterruptedException {
		outkey.setName(max_key);
		outkey.setPay(max_val);
		// 统一输出
		context.write(outkey, NullWritable.get());

		outkey.setName(min_key);
		outkey.setPay(min_val);
		// 统一输出
		context.write(outkey, NullWritable.get());

	}

}


	//reduce
	private static class MyReducer extends Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable> {

		private DrugWritable outkey = new DrugWritable();

		//多目录输出
		private MultipleOutputs<DrugWritable,NullWritable> outputs = null;

		@Override
		protected void setup(Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// 实例化 多目录输出第一步
			outputs = new MultipleOutputs<>(context);
		}

		// 冒泡排序
		private String current_key = "";
		private Double current_val = 0D;
		private String max_key = "";
		private Double max_val = 0D;
		private String min_key = "";
		private Double min_val = 0D;

		@Override
		protected void reduce(DrugWritable key, Iterable<NullWritable> values, Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {

			current_key = key.getName();
			current_val = key.getPay();
			
			if(current_val >= max_val){
				max_key = current_key;
				max_val = current_val;
			}
			if(current_val <= min_val || min_val == 0){
				min_val = current_val;
				min_key = current_key;
			}
		}

		//MapReduce使用cleanup()方法实现排序筛选后输出

		/**
		 * 背景
		 * MapReduce的map和reduce方法有一个局限性，就是map()方法每次只处理一行，而reduce()方法每次只处理一组。
		 * 并且reduce一般都是将处理每一组数据后的结果都写出。但有时候想要只输出一部分结果，
		 * 比如在Wordcount程序中，想要输出单词数量前三的统计信息，这时就可以用cleanup()方法来实现。
		 */
		@Override
		protected void cleanup(Reducer<DrugWritable, NullWritable, DrugWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {

			outkey.setName(max_key);
			outkey.setPay(max_val);
			//多目录输出 第二步
			outputs.write(outkey, NullWritable.get(), "max/maxval");
			//统一输出
			context.write(outkey, NullWritable.get());
			outkey.setName(min_key);
			outkey.setPay(min_val);
			//统一输出
			context.write(outkey, NullWritable.get());
			// 多目录输出
			outputs.write(outkey, NullWritable.get(), "min/minval");
			// 如果用完了记得关闭这个流 要不报错
			if(null != outputs){
				outputs.close();
			}
		}
	}

	@Override
	public int run(String[] args) throws Exception {

		// 创建本次的job
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "distinct");

		// 设置 job
		// 第一步设置类
		job.setJarByClass(MyMRDemo7.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		//3.使用combiner处理输出给到reduce
		//这里不要设置为reduce,否则会产生多个输出文件
		job.setCombinerClass(MyCombiner.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(DrugWritable.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		// 第二步 设置路径
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			// hadoop fs -rm -r -skipTrash --> 递归删除
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行

			boolean con = job.waitForCompletion(true);
			String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
			System.out.println(msg);
		return 0;
	}

	public static void main(String[] args) {

		try {
			System.exit(ToolRunner.run(new MyMRDemo7(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
