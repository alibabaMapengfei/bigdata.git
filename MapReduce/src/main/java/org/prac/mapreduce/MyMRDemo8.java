package org.prac.mapreduce;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.prac.entity.AreaWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *
 * 大小表 semijoin 实现
 * 1.管理员身份启动软件
 * 2.参数语法 :
 * -Dmapreduce.job.cache.files=小表所在的位置 项目的输入路径 项目的输出路径
 * 3.map实现setup方法 通过分布式缓存加载到map里面,
 * 4.给到map方法然后输出数据
 */
public class MyMRDemo8 extends Configured implements Tool {

	private static class MyMapper extends Mapper<LongWritable, Text, AreaWritable, NullWritable> {
		
		// 加载小表的数据 HDFS 分布式缓存 --> 发送你mapper执行的本地目录 --> 通过文件名获取文件

		//解析到的小表数据file1 放到map里面
		private static Map<Integer,String> areaMap = new HashMap<Integer,String>(16);
		@Override
		protected void setup(Mapper<LongWritable, Text, AreaWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// 获取文件位置 小表
			URI uri =  context.getCacheFiles()[0];
			// 获取文件名
			String fileName = uri.getPath().toString().substring(uri.getPath().toString().lastIndexOf("/")+1);
			// 解析file1
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"UTF-8"));
			String line = null;
			while((line = reader.readLine()) != null){
				areaMap.put(Integer.parseInt(line.split("\t")[0].trim()),line.split("\t")[1].trim());
			}

			// 关闭
			if(null != reader){
				reader.close();
			}
			
		}
		
		private String[] strs = null;
		private String aname = null;
		private AreaWritable outkey = new AreaWritable();
		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, AreaWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// 拆分数据
			strs = value.toString().split("\t");
			// 防御式编程
			if(null != strs && strs.length == 3){
				
				aname = areaMap.get(Integer.parseInt(strs[0].trim()));
				
				if(null != aname && !"".equals(aname.trim())){
					// 正常
					outkey.setAid(Integer.parseInt(strs[0].trim()));
					outkey.setAname(aname);
					outkey.setYear(Integer.parseInt(strs[1].trim()));
					outkey.setCount(Long.parseLong(strs[2].trim()));
					context.write(outkey, NullWritable.get());
				}
			}
		}
	}

	@Override
	public int run(String[] args) throws Exception {

		// 获取配置文件的加载对象
		Configuration conf = this.getConf();

		// 设置方法的返回值
		Job job = Job.getInstance(conf, "semijoin");

		// 设置类
		job.setJarByClass(MyMRDemo8.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setNumReduceTasks(0);
		job.setOutputKeyClass(AreaWritable.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		// 第二步 设置路径
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			// hadoop fs -rm -r -skipTrash --> 递归删除
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行
		long start = System.currentTimeMillis();
		boolean con = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
		System.out.println(msg);
		System.out.println("JOB_COST : " + ((end - start) / 1000) + " SECONDS!");

		return 0;
	}

	public static void main(String[] args) {
		try {
			System.exit(ToolRunner.run(new MyMRDemo8(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
