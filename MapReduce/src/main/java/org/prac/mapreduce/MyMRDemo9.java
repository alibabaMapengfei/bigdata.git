package org.prac.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import util.MyIntComparator;

/**
 * mapreduce实现单reduce排序查询
 * 简单类型key比较
 * 正序输出:默认就是正序
 *倒序输出:
 * 1.通过自定义排序类
 * 2.job.setSortComparatorClass(MyIntComparator.class);
 */
public class MyMRDemo9 extends Configured implements Tool {

	
	private static class MyMapper extends Mapper<LongWritable, Text, IntWritable, Text>{
		
		private IntWritable outkey = new IntWritable();
		private Text outval = new Text();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			strs = value.toString().split("\t");
			outkey.set(Integer.parseInt(strs[0].trim()));
			outval.set(strs[1].trim());
			context.write(outkey, outval);
		}
	}
	
	private static class MyReducer extends Reducer<IntWritable, Text, IntWritable, Text>{
		
		
		private Text outval = new Text();
		
		@Override
		protected void reduce(IntWritable key, Iterable<Text> values,
				Reducer<IntWritable, Text, IntWritable, Text>.Context context) throws IOException, InterruptedException {
			
			outval.set(values.iterator().next().toString());
			context.write(key,outval);
		}
	}
	
	
	
	@Override
	public int run(String[] args) throws Exception {
		
		// 获取配置文件
		Configuration conf = this.getConf();
		// 创建job
		Job job = Job.getInstance(conf, "desc sort");
		
		
		// 类设置
		job.setJarByClass(MyMRDemo9.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setSortComparatorClass(MyIntComparator.class);
		
		// 第二步 设置路径
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(out)) {
			// hadoop fs -rm -r -skipTrash --> 递归删除
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		// 第三步 设置执行
		long start = System.currentTimeMillis();
		boolean con = job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (con ? "OK!" : "FAIL!");
		System.out.println(msg);
		System.out.println("JOB_COST : " + ((end - start) / 1000) + " SECONDS!");
		
		return 0;
	}
	
	public static void main(String[] args) {
		
		try {
			System.exit(ToolRunner.run(new MyMRDemo9(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
