package org.prac.mapreduce;

import java.io.IOException;

import org.prac.entity.AreaWriteable11;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * 二次排序
 *1.利用partitioner
 * 2.group分组
 */
public class MyMRDemo12 extends Configured implements Tool {
	
	private static final String SPLIT_STR1 = "\t";
	
	private static class MyMapper extends Mapper<LongWritable, Text, AreaWriteable11, NullWritable>{

		// 定义map需要用到的变量
		private AreaWriteable11 outkey = new AreaWriteable11();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, AreaWriteable11, NullWritable>.Context context)
				throws IOException, InterruptedException {
			
			// 拆分数据
			strs = value.toString().split(SPLIT_STR1);
			// 防御式编程
			if(null != strs && strs.length == 2){
				
				outkey.setUserName(strs[0].trim());
				outkey.setUserCost(Long.parseLong(strs[1].trim()));
				// 输出
				context.write(outkey, NullWritable.get());
			}
		}
	}
	
	private static class MyPartitioner extends Partitioner<AreaWriteable11, NullWritable>{

		@Override
		public int getPartition(AreaWriteable11 key, NullWritable value, int numPartitions) {

			if(key.getUserName().toLowerCase().startsWith("hadoop")){
				return 0;
			}else{
				return 1;
			}
		}
	}

	private static class Mygroup1 extends WritableComparator{

		public Mygroup1() {
			super(AreaWriteable11.class, true);
		}

		@Override
		public int compare(WritableComparable a, WritableComparable b) {

			AreaWriteable11 aa = (AreaWriteable11)a;
			AreaWriteable11 ab = (AreaWriteable11)b;
			return aa.getUserName().compareTo(ab.getUserName());
		}
	}
	
	
	
	@Override
	public int run(String[] args) throws Exception {
		
		// 创建配置文件加载对象
		Configuration conf = this.getConf();
		
		// 编写job
		Job job = Job.getInstance(conf, "secondary_sort");
		
		// 类设置
		job.setJarByClass(MyMRDemo12.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setNumReduceTasks(2);
		job.setPartitionerClass(MyPartitioner.class);
		job.setOutputKeyClass(AreaWriteable11.class);
		job.setOutputValueClass(NullWritable.class);
		// 分组比较器设置 它不真正进行排序实现 但是可以帮助reduce 将相同的key放到一次处理 实现reduce 并行化数据处理
		job.setGroupingComparatorClass(Mygroup1.class);//自定义组内排序
		//job.setSortComparatorClass(Mygroup1.class);//自定义分组
		job.setOutputFormatClass(TextOutputFormat.class);
		
		
		// 路径设置
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		
		// 保证输出路径没有
		FileSystem fs = FileSystem.get(conf);
		
		if(fs.exists(out)){
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		
		// 运行设置
		long start = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (job.waitForCompletion(true)?"OK!":"FAIL!");
		long end = System.currentTimeMillis();
		System.out.println(msg);
		System.out.println("JOB_COST : " + (end - start)/1000 + " SECONDS!");
		// 返回
		return 0;
	}

	
	public static void main(String[] args) {
		
		try {
			System.exit(ToolRunner.run(new MyMRDemo12(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
