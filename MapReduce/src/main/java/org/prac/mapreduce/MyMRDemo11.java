package org.prac.mapreduce;

import java.io.IOException;
import org.prac.entity.StudentWritable1;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * 多reduce输出,输出并排序
 * 1.StudentWritable1序列化类
 * 2.设置多个reduce输出, 使用partitioner进行数据判断,分别存入四个reduce的key
 */

//1	盖谦	2001-01-14	11480630	1	东城区第1中学	1	东城区	540
public class MyMRDemo11 extends Configured implements Tool {
	
	private static final String SPLIT_STR1 = "\t";
	
	private static class MyMapper extends Mapper<LongWritable, Text, StudentWritable1, NullWritable>{
		
		
		// 定义map需要用到的变量
		private StudentWritable1 outkey = new StudentWritable1();
		private String[] strs = null;
		
		@Override
		protected void map(LongWritable key, Text value,
				Mapper<LongWritable, Text, StudentWritable1, NullWritable>.Context context)
				throws IOException, InterruptedException {
			
			// 拆分数据
			strs = value.toString().split(SPLIT_STR1);
			// 防御式编程
			if(null != strs && strs.length == 9){
				
				outkey.setAname(strs[7].trim());
				outkey.setBirthday(strs[2].trim());
				outkey.setExamno(strs[3].trim());
				outkey.setScname(strs[5].trim());
				outkey.setScore(Integer.parseInt(strs[8].trim()));
				outkey.setSname(strs[1].trim());
				// 输出
				context.write(outkey, NullWritable.get());
			}
			
		}
		
	}
	
	private static class MyPartitioner extends Partitioner<StudentWritable1, NullWritable>{

		@Override
		public int getPartition(StudentWritable1 key, NullWritable value, int numPartitions) {
			
			// 定义方法的返回值
			//分别会找到4个reduce输出
			int reduceID = 3;
			
			if(key.getScore() >= 550){
				reduceID = 0;
			}else if(key.getScore() >= 450 && key.getScore() < 550){
				reduceID = 1;
			}else if(key.getScore() >= 250  && key.getScore() < 450){
				reduceID = 2;
			}
			// 返回
			return reduceID;
		}
		
	}
	
	
	
	
	@Override
	public int run(String[] args) throws Exception {
		
		// 创建配置文件加载对象
		Configuration conf = this.getConf();
		
		// 编写job
		Job job = Job.getInstance(conf, "many_reduce_sort");
		
		// 类设置
		job.setJarByClass(MyMRDemo11.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setNumReduceTasks(4);
		job.setPartitionerClass(MyPartitioner.class);
		job.setOutputKeyClass(StudentWritable1.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		
		// 路径设置
		Path in = new Path(args[0]);
		Path out = new Path(args[1]);
		
		// 保证输出路径没有
		FileSystem fs = FileSystem.get(conf);
		
		if(fs.exists(out)){
			fs.delete(out, true);
			System.out.println(job.getJobName() + "'s output dir is deleted!");
		}
		
		FileInputFormat.addInputPath(job, in);
		FileOutputFormat.setOutputPath(job, out);
		
		// 运行设置
		long start = System.currentTimeMillis();
		String msg = "JOB_STATUS : " + (job.waitForCompletion(true)?"OK!":"FAIL!");
		long end = System.currentTimeMillis();
		System.out.println(msg);
		System.out.println("JOB_COST : " + (end - start)/1000 + " SECONDS!");
		// 返回
		return 0;
	}

	
	public static void main(String[] args) {
		
		try {
			System.exit(ToolRunner.run(new MyMRDemo11(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
