package com.hnxy.mr;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.hnxy.utils.BaseMR;
import com.hnxy.utils.EtlUtils;
import com.hnxy.utils.MyConstant;
import com.hnxy.utils.UserAgentUtil;
import com.hnxy.utils.Utils;

public class TextFileToAvro extends BaseMR {

	
	// 创建文件转换对象
    private static Parser p1 = null;

    // 创建avro的结构对象
    private static Schema schema = null;

    static {

        try {
            p1 = new Parser();
            // 加载avsc文件
            InputStream in = TextFileToAvro.class.getClassLoader().getResourceAsStream("nginx_log_avro.avsc");
            // 创建结构对象
            schema = p1.parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
	
	
	
	private static class MyMapper extends Mapper<LongWritable, Text, AvroKey<GenericRecord>, NullWritable>{
		
		// 定义map需要用到的变量
		private GenericRecord outkey = null;
		private String[] strs = null;
		private String ip,acctime,uid,country,referer,osfamily,uafamily;
		private DateFormat df1 = new SimpleDateFormat("yyyyMMddHHmmss");
        private DateFormat df2 = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss", Locale.ENGLISH);
		private List<String> list = null;
		
		
		@Override
		protected void map(LongWritable key, Text value,
				Context context)
				throws IOException, InterruptedException {
			
			// 进行数据的拆分
			strs = value.toString().split(MyConstant.SPLIT_STR2);
			
			// 进行字段的整理
			context.getCounter("Line Quality Info", "Total Line Count").increment(1);
			if(null != strs & strs.length == 9){
				// 正常的数据
				context.getCounter("Line Quality Info", "Nice Line Count").increment(1);
				ip = strs[0].trim();
				try {
					acctime = df1.format((df2.parse(strs[2].trim()))); // 04/Aug/2020:19:05:04 +0800
				} catch (ParseException e) {
					acctime = "ERROR";
				}
				list = EtlUtils.getUidAndCountryByRequest(strs[3].trim());
				if(!(null != list && list.size() == 2)){
					context.getCounter("Line Quality Info", "Country Bad Line Count").increment(1);
					return;
				}
				// 开始进行 uid和country的解析
				uid = list.get(0);
				country = list.get(1);
				referer = strs[6].trim();
				if(Utils.isEmpty(referer)){
					context.getCounter("Line Quality Info", "Referer Bad Line Count").increment(1);
					return;
				}
				if(Utils.isEmpty(strs[7])){
					context.getCounter("Line Quality Info", "UserAgent Bad Line Count").increment(1);
					return;
				}
				// 操作系统名称
				osfamily = UserAgentUtil.getUserAgentParams(strs[7].trim()).get("osfamily");
				// 浏览器名称
				uafamily = UserAgentUtil.getUserAgentParams(strs[7].trim()).get("uafamily");
				// 设置封装avro
				outkey = new GenericData.Record(schema);
				// 添加数据
				outkey.put("ip", ip);
				outkey.put("acctime", acctime);
				outkey.put("uid", uid);
				outkey.put("country", country);
				outkey.put("referer", referer);
				outkey.put("osfamily", osfamily);
				outkey.put("uafamily", uafamily);
				
				// 输出数据
				context.write(new AvroKey<GenericRecord>(outkey), NullWritable.get());
				
				
			}else{
				// 不正常的数据
				context.getCounter("Line Quality Info", "Bad Line Count").increment(1);
				return;
			}
			
			
			
			
			
			
		}
		
		
		
	}
	
	
	@Override
	public Job getJob(Configuration conf) throws Exception {
		
		// 创建方法的返回值
		Job job = Job.getInstance(conf, this.getJobNameWithTaskID());
		
		// job 设置
		job.setJarByClass(TextFileToAvro.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setMapperClass(MyMapper.class);
		job.setNumReduceTasks(0);
		job.setOutputKeyClass(AvroKey.class);
		job.setOutputValueClass(NullWritable.class);
		job.setOutputFormatClass(AvroKeyOutputFormat.class);
		
		// 设置avsc
		AvroJob.setMapOutputKeySchema(job, schema);
		
		// 路径设置
		FileInputFormat.addInputPath(job, new Path(conf.get(MyConstant.TASK_INPUT)));
		FileOutputFormat.setOutputPath(job, this.getOutPutPath());
		
		// 返回
		return job;
	}

	@Override
	public String getJobName() {
		return "t2a";
	}

}
