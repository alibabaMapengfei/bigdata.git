package com.hnxy.utils;

/**
 * 常量设置类
 * @author Su
 *
 */
public class MyConstant {

	
	// 定义公共的分割符号
	public static final String SPLIT_STR1 = "\t";
	public static final String SPLIT_STR2 = "\001";
	// 自定义-D参数传递
	public static final String TASK_USER = "mymr.task.user";
	public static final String TASK_INPUT = "mymr.task.input";
	
	// 设置公共输出目录
	public static final String BASE_OUTPUT_PATH = "/mapreduce/task/output/";
	
	
	
	
}
