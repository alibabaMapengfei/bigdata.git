package com.hnxy.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;

/**
 * 获取job运行结果的工具类
 * @author Su
 *
 */
public class JobRunnerUtilResult {

	
	private Boolean isOK;		// 是否运行成功
	private String runTime;		// 运行时间
	private Map<String,Counters> countersMap = new HashMap<String, Counters>();
	
	public Boolean getIsOK() {
		return isOK;
	}
	public void setIsOK(Boolean isOK) {
		this.isOK = isOK;
	}
	public String getRunTime() {
		return runTime;
	}
	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}
	public Map<String, Counters> getCountersMap() {
		return countersMap;
	}
	public void setCountersMap(Map<String, Counters> countersMap) {
		this.countersMap = countersMap;
	}
	
	// 赠送如何解析counter的方法
	
	// 1. 获取某个job的counters
	public Counters getCountersByJob(String jobName){
		return countersMap.get(jobName);
	}
	
	public Counters getCountersByJob(ControlledJob job){
		return countersMap.get(job.getJob().getJobName());
	}
	
	public Counters getCountersByJob(Job job){
		return countersMap.get(job.getJobName());
	}
	
	// 2. 如何获取某个counters中的counter
	public Counter getCountByCounters(ControlledJob job,String groupName,String counterName){
		Counters countes = this.getCountersByJob(job);
		return countes.findCounter(groupName, counterName);
	}
	
	// 3. 如何获取某个counter的值
	public Long getCounterValByCounter(ControlledJob job,String groupName,String counterName){
		Counter counter = this.getCountByCounters(job, groupName, counterName);
		if(Utils.isNotEmpty(counter)){
			return counter.getValue();
		}else{
			return 0L;
		}
	}
	
	
	
}
