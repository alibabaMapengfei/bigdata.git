package com.hnxy.utils;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * 数据库连接工具类
 * @author Su
 *
 */
public class JdbcUtil {

    // 创建数据库的连接对象
    private static final String CONN_DRIVER = "com.mysql.jdbc.Driver";
    private static final String CONN_URL = "jdbc:mysql://192.168.92.30:3306/etl_db?useEncoding=true&characterEncoding=UTF-8";
    private static final String CONN_USER = "etl";
    private static final String CONN_PASSWORD = "000000";
    
    
    // 创建数据源对象
    private static DruidDataSource dataSource = new DruidDataSource();
    
    // 赋值
    static{
        dataSource.setDriverClassName(CONN_DRIVER);
        dataSource.setUrl(CONN_URL);
        dataSource.setUsername(CONN_USER);
        dataSource.setPassword(CONN_PASSWORD);
    }
    
    // 获取数据源
    public static DruidDataSource getDataSource() {
        return dataSource;
    }
}
