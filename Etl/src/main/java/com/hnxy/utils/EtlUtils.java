package com.hnxy.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EtlUtils {
    
    public static List<String> getUidAndCountryByRequest(String request) {
        
        // 创建方法的返回值
        List<String> strs = new ArrayList<String>();
        
        // 按指定模式在字符串查找
        String line = request;
        String pattern = "((uid=\\w+)&(country=(\\w+|\\s+)))";

        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);

        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        if (m.find()) {
            String uid = m.group(2).replace("uid=", "");
            String country = m.group(3).replace("country=", "");
            if(Utils.isNotEmpty(uid)){
                strs.add(uid);
            }
            if(Utils.isNotEmpty(country)){
                strs.add(country);
            }
            
            
        } else {
            System.out.println("NO MATCH");
        }
        // 返回
        return strs;
    }
    
    
    
}
