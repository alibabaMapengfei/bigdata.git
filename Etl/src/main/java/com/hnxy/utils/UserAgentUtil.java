package com.hnxy.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cz.mallat.uasparser.OnlineUpdater;
import cz.mallat.uasparser.UASparser;
import cz.mallat.uasparser.UserAgentInfo;

/**
 * 
 * @author Su
 *
 */
public class UserAgentUtil {

    // 创建 userAgent 解析实例
    private static UASparser parser = null;
    
    
    static {
        
        try {
            // 创建解析对象
            parser = new UASparser(OnlineUpdater.getVendoredInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * 获取UserAgent相关信息
     * osfamily : 操作系统家族名称
     * osname : 操作系统名称
     * uaname : 浏览器名称(带版本的)
     * uafamily : 浏览器名称(不带版本的)
     * browserversion : 浏览器版本
     * devicetype : 设备类型
     * type : 访问类型
     * 
     * @param userAgentStr
     * @return
     */
    public static Map<String, String> getUserAgentParams(String userAgentStr) {

        // 创建方法的返回值
        Map<String, String> map = new HashMap<String, String>();

        try {
            
            UserAgentInfo userAgentInfo = UserAgentUtil.parser.parse(userAgentStr);
            map.put("osfamily", userAgentInfo.getOsFamily());
            map.put("osname", userAgentInfo.getOsName());
            map.put("uaname", userAgentInfo.getUaName());
            map.put("type", userAgentInfo.getType());
            map.put("uafamily", userAgentInfo.getUaFamily());
            map.put("browserversion", userAgentInfo.getBrowserVersionInfo());
            map.put("devicetype", userAgentInfo.getDeviceType());
            
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 返回
        return map;

    }
    
    
}
