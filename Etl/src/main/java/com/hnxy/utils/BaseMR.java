package com.hnxy.utils;

import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;

/**
 * 所有MR的基类
 * @author Su
 *
 */
public abstract class BaseMR {
	
	// 创建一个公共的conf对象
	private static Configuration conf;
	// set方法
	public static void setConf(Configuration conf) {
		BaseMR.conf = conf;
	}
	
	// 编写一个公共的提供 ControlledJob 的方法
	
	/**
	 * 公共的获取 ControlledJob 的方法
	 * @return
	 * @throws Exception
	 */
	public ControlledJob getControlledJob()throws Exception{
		
		// 编写方法的返回值
		ControlledJob cjob = new ControlledJob(conf);
		
		// 将删除输出目录部分的东西贡献出来形成公共部分
		FileSystem fs = FileSystem.get(conf);
		// 设定输出目录
		Path out = this.getOutPutPath();
		// 判断一下
		if(fs.exists(out)){
			fs.delete(out, true);
			System.out.println(this.getJobName() + "'s output dir is deleted!");
		}
		
		// 获取job
		Configuration myConf = new Configuration();
		// 循环公共的conf --> 赋给 私有conf
		for (Entry<String, String> entry : conf) {
			myConf.set(entry.getKey(), entry.getValue());
		}
		Job job = this.getJob(myConf);
		
		// 绑定
		cjob.setJob(job);
		// 返回
		return cjob;
		
		
	}
	// 获取mrjob
	public abstract Job getJob(Configuration conf) throws Exception;
	
	// 获取mr的任务名称
	public abstract String getJobName();
	
	// -Dmymr.task.user=suniu
	public String getJobNameWithTaskID(){
		return this.getJobName() + "_" + conf.get(MyConstant.TASK_USER);
	}
	
	// 都往这里输出
	public Path getOutPutPath() {
		return new Path(MyConstant.BASE_OUTPUT_PATH + this.getJobNameWithTaskID());
	}
	
	
	
	

}
