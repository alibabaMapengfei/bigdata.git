package com.hnxy.bin;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.lib.jobcontrol.JobControl;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.hnxy.mr.TextFileToAvro;
import com.hnxy.utils.BaseMR;
import com.hnxy.utils.JdbcUtil;
import com.hnxy.utils.JobRunnerUtil;
import com.hnxy.utils.JobRunnerUtilResult;

public class MyRunner extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		
		// 创建配置文件的加载对象
		Configuration conf = this.getConf();
		// BASEMR
		BaseMR.setConf(conf);
		// 创建job
		TextFileToAvro t2a = new TextFileToAvro();
		// 创建任务工作链
		JobControl ctrl = new JobControl("ctrl1");
		// 绑定任务
		ctrl.addJob(t2a.getControlledJob());
		// 执行
		JobRunnerUtilResult result = JobRunnerUtil.run(ctrl);
		// 判断是否执行成功
		if(result.getIsOK()){
			
			// 进行counter入库
			Map<String,Counters> map = result.getCountersMap();
			
			Counters counters = null;
			QueryRunner qr = new QueryRunner(JdbcUtil.getDataSource());
			String sql = "delete from counter_info where c_time like concat(?,'%')";
			String sql1 = "insert into counter_info (c_name,c_val,c_group,c_time,c_author) values (?,?,?,?,?)";
			for(Entry<String, Counters> entry : map.entrySet()){
				// 获取counters
				counters = entry.getValue();
				// 解析counters
				for(CounterGroup cg : counters){
					
					// 判断是不是我们需要的counter
					if(cg.getDisplayName().equals("Line Quality Info")){
						
						// 开始解析
						// 首先删除这个小时重复的counter
						qr.update(sql,new Object[]{new SimpleDateFormat("yyyy-MM-dd HH").format(new java.util.Date(System.currentTimeMillis()-3600000))});
						
						for(Counter counter : cg){
							// 开始添加
							qr.update(sql1,new Object[]{counter.getDisplayName(),counter.getValue(),cg.getDisplayName(),new SimpleDateFormat("yyyy-MM-dd HH").format(new java.util.Date(System.currentTimeMillis()-3600000)),"suniu"});
						}
						
					}
					
				}
				
				
				
			}
			System.out.println("JOB_STATUS : OK!");
			
		}else{
			System.out.println("JOB_STATUS : FAIL");
		}
		
		System.out.println("JOB_COST : " + result.getRunTime());
		
		
		return 0;
	}
	
	
	public static void main(String[] args) {
		try {
			System.exit(ToolRunner.run(new MyRunner(), args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
