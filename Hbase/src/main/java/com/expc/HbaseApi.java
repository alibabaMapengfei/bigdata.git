package com.expc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author : xinniu
 * @describe :连接kerberos环境下hbase
 */
public class HbaseApi {
    static Logger logger = Logger.getLogger(HbaseApi.class);
    static Configuration conf = HBaseConfiguration.create();
    static TableName tablename = TableName.valueOf("xiniu:apitable");

    public static void main(String[] args) {
        String krb5Path = "src/main/conf/krb5.conf";
        String principal = "hbase@HAINIU.COM";
        String keytabPath = "src/main/conf/hbase.keytab";
        Configuration configuration = HBaseConfiguration.create();
        kerberosAuth(configuration,krb5Path,principal,keytabPath);

        //create hbase table by new hbase api
//        createHbaseTable(configuration);
//        createTable();
//        putTable();
        putsTable();
        scanTable();
//        getTable();
//        deleteAPI();
//        deleteCF();
//        deleteTable();
    }
    /**
     * 删除列族
     */
    private static void deleteCF() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表管理对象
                HBaseAdmin admin = (HBaseAdmin)conn.getAdmin();
        )
        {
            admin.deleteColumn(tablename,Bytes.toBytes("cf1"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除表操作
     */
    private static void deleteTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表管理对象
                HBaseAdmin admin = (HBaseAdmin)conn.getAdmin();
        )
        {
            //下线表
            admin.disableTable(tablename);
            admin.deleteTable(tablename);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除行
     */
    private static void deleteAPI() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表操作对象
                HTable table = (HTable) conn.getTable(tablename);
        )
        {
            //要删除的行
            Delete delete = new Delete(Bytes.toBytes("x0003"));
            //要删除的列，指定版本号
            delete.addColumn(Bytes.toBytes("cf1"),Bytes.toBytes("address"),1621784365953L);

            table.delete(delete);

            logger.info("x0003行删除完成");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取某一行，对标shell中的get命令
     */
    private static void getTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表操作对象
                HTable table = (HTable) conn.getTable(tablename);
        )
        {
            //获取row对象
            Get get = new Get(Bytes.toBytes("x0003"));
            //添加查询列，如果不指定列则查询该行所有数据
//            get.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("name"));

            Result result = table.get(get);
            printResult(result);

            //也可以使用这种方式获取单个列
            System.out.println(Bytes.toString(result.getValue(Bytes.toBytes("cf1"), Bytes.toBytes("name"))));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void printResult(Result result) {
        StringBuilder sbd = new StringBuilder();
        Cell[] cells = result.rawCells();
        for (Cell cell : cells) {
            byte[] row = CellUtil.cloneRow(cell);
            byte[] cf = CellUtil.cloneFamily(cell);
            byte[] qualifier = CellUtil.cloneQualifier(cell);
            long version = cell.getTimestamp();
            byte[] value = CellUtil.cloneValue(cell);
            sbd.append(Bytes.toString(row))
                    .append("\t")
                    .append("cf:")
                    .append(Bytes.toString(cf))
                    .append("\t")
                    .append("qualifier:")
                    .append(Bytes.toString(qualifier))
                    .append("\t")
                    .append("version:")
                    .append(version)
                    .append("\t")
                    .append("value:")
                    .append(Bytes.toString(value))
                    .append("\n");
        }
        logger.info("\n输出:\n" + sbd.toString());
    }

    /**
     * 扫描全表、范围查询，对标shell中的scan命令
     */
    private static void scanTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表操作对象
                HTable table = (HTable) conn.getTable(tablename);
        )
        {
            //创建扫描对象，入参为startrow与endrow，前闭后开
            Scan scanbyrow = new Scan(Bytes.toBytes("x0001"), Bytes.toBytes("x0003"));
            //不限制开始结束row则为全表扫描
            Scan scanall = new Scan();

            //设置版本号，如果超出列族最大版本支持则只能返回列族支持的版本数
            scanbyrow.setMaxVersions(1);

            //获取查询结果
            ResultScanner resultsbyrow = table.getScanner(scanbyrow);
            ResultScanner resultsall = table.getScanner(scanall);

            //遍历查询结果并输出
            printResult(resultsbyrow);
//            printResult(resultsall);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param results
     * 遍历输出查询结果
     */
    private static void printResult(ResultScanner results) {
        int index = 1;
        for (Result result : results) {
            StringBuilder sbd = new StringBuilder();
            Cell[] cells = result.rawCells();
            for (Cell cell : cells) {
                byte[] row = CellUtil.cloneRow(cell);
                byte[] cf = CellUtil.cloneFamily(cell);
                byte[] qualifier = CellUtil.cloneQualifier(cell);
                long version = cell.getTimestamp();
                byte[] value = CellUtil.cloneValue(cell);
                sbd.append(Bytes.toString(row))
                        .append("\t")
                        .append("cf:")
                        .append(Bytes.toString(cf))
                        .append("\t")
                        .append("qualifier:")
                        .append(Bytes.toString(qualifier))
                        .append("\t")
                        .append("version:")
                        .append(version)
                        .append("\t")
                        .append("value:")
                        .append(Bytes.toString(value))
                        .append("\n");
            }
            logger.info("\n输出第"+index+"行:\n" + sbd.toString());
            index++;
        }
    }

    /**
     * 写多行数据，对标shell中的put
     */
    private static void putsTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表操作对象
                HTable table = (HTable) conn.getTable(tablename);
        )
        {
            //创建put对象，传入的值为rowkey
            Put put1 = new Put(Bytes.toBytes("x0002"));

            //向put对象中添加值
            put1.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("name"), Bytes.toBytes("user2"));
            put1.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("age"), Bytes.toBytes("22"));
            put1.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("address"), Bytes.toBytes("shanghai"));

            //创建put对象，传入的值为rowkey
            Put put2 = new Put(Bytes.toBytes("x0003"));

            //向put对象中添加值
            put2.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("name"), Bytes.toBytes("user3"));
            put2.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("age"), Bytes.toBytes("23"));
            put2.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("address"), Bytes.toBytes("hainiu"));

            ArrayList<Put> puts = new ArrayList<>();

            puts.add(put1);
            puts.add(put2);

            table.put(puts);
            logger.debug("写多行完成");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 写一行数据，对标shell中的put命令
     */
    private static void putTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表操作对象
                HTable table = (HTable) conn.getTable(tablename);
        )
        {
            //创建put对象，传入的值为rowkey
            Put put = new Put(Bytes.toBytes("x0001"));

            //向put对象中添加值
            put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("name"), Bytes.toBytes("user1"));
            put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("age"), Bytes.toBytes("21"));
            put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("address"), Bytes.toBytes("beijing"));

            table.put(put);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建表，对标shell中的create命令
     */
    private static void createTable() {
        try (
                //获取连接
                Connection conn = ConnectionFactory.createConnection(conf);

                //表管理对象
                HBaseAdmin admin = (HBaseAdmin)conn.getAdmin();
        )
        {
            if (admin.tableExists(tablename)){
                System.out.println(tablename + " is exists");
                conn.close();
                return;
            }

            //创建表描述对象
            HTableDescriptor table = new HTableDescriptor(tablename);

            //创建列族描述对象
            HColumnDescriptor cf1 = new HColumnDescriptor(Bytes.toBytes("cf1"));
            HColumnDescriptor cf2 = new HColumnDescriptor(Bytes.toBytes("cf2"));

            //注册列族到表
            table.addFamily(cf1);
            table.addFamily(cf2);

            //使用表管理对象创建表
            admin.createTable(table);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 使用hbase新api建表
     * @param configuration hbase配置文件
     */
    private static void createHbaseTable(Configuration configuration) {
        try (
                Connection connection = ConnectionFactory.createConnection(configuration);
                HBaseAdmin admin = (HBaseAdmin)connection.getAdmin();
                ){

            TableName tableName = TableName.valueOf(Bytes.toBytes("xiniu:test333"));

            if (admin.tableExists(tableName)){
                logger.error("tableName is exists!");
                return;
            }
            TableDescriptorBuilder table = TableDescriptorBuilder.newBuilder(tableName);

            ColumnFamilyDescriptorBuilder cf1 = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes("cf1"));
            ColumnFamilyDescriptorBuilder cf2 = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes("cf2"));

            table.setColumnFamily(cf1.build());
            table.setColumnFamily(cf2.build());

            admin.createTable(table.build());

        } catch (Exception e) {
            logger.error(e);
        }
    }

    /**
     * kerberos认证
     * @param configuration hadoop配置文件
     * @param krb5Path kerberos配置文件(krb5.conf)路径
     * @param principal 认证主体名
     * @param keytabPath keytab文件路径
     */
    public static void kerberosAuth(Configuration configuration,String krb5Path,String principal,String keytabPath){
        System.setProperty("java.security.krb5.conf",krb5Path);
        configuration.set("hadoop.security.authentication","Kerberos");
        UserGroupInformation.setConfiguration(configuration);
        try {
            UserGroupInformation.loginUserFromKeytab(principal,keytabPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}