package com.expc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;

import java.io.IOException;

/**
 * @Description（描述）: Hbase
 * @Version（版本）: 1.0
 * @Author（创建者）: ALIENWARE
 * @Date（创建时间）: Created in 2021/7/8.
 * @ * * * * * * * * * * * * * @
 */
public class hbasetest {
	static {
		//kerberos安全认证使用
		String keytabPath = "src/main/conf/hbase.keytab";
		String krb5Path = "src/main/conf/krb5.conf";
		String principal = "hbase@HAINIU.COM";
	}
	
	/**
	 * hbase相关操作
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration conf = HBaseConfiguration.create();
		//设置要操作的表名
		TableName tableName = TableName.valueOf("ns:api1");
		//创建表
		//createTable(tableName);
		//插入数据
		//put(conf, tableName);
		//插入多条数据
		//puts(conf,tableName);
		//scan表
		//scantable(conf,tableName);
		//get获取数据，指定rowkey的方式查询
		//getApi(conf,tableName);
		//删除操作
		//deleteApi(conf,tableName);
		//删除表
		deletaTableApi(conf, tableName);
	}
	
	
	/**
	 * 删除表
	 *
	 * @param conf
	 * @param tableName
	 */
	private static void deletaTableApi(Configuration conf, TableName tableName)
	{
		try (
				//创建链接
				Connection conn = ConnectionFactory.createConnection();
				//创建表管理对象，表管理对象主要用于创建表、删除表、删除列族
				HBaseAdmin admin = (HBaseAdmin) conn.getAdmin();
			)
		{
			if (admin.tableExists(tableName)){
				//下线表
				admin.disableTable(tableName);
				admin.deleteTable(tableName);
				System.out.println(tableName + ":删除完成");
			} else {
				System.out.println(tableName + ": 表不存在！");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
