package com.bigdata.hive.function;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.parse.SemanticException;
import org.apache.hadoop.hive.ql.udf.generic.AbstractGenericUDAFResolver;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDAFEvaluator;
import org.apache.hadoop.hive.serde2.lazy.LazyInteger;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.io.IntWritable;

public class SumUDAF extends AbstractGenericUDAFResolver {

  @Override
  public GenericUDAFEvaluator getEvaluator(TypeInfo[] info) throws SemanticException {
    // 校验函数输入参数  和 返回 具体能实现udaf函数的Evaluator对象
    // 校验函数入参的数量
    if (info.length != 1) {
      throw new UDFArgumentException("input param must one!");
    }

    // 校验函数入参的大类类型
    ObjectInspector inspector = (ObjectInspector) info[0];
    // 获取大类类型的方法： inspector.getCategory()
    // 获取小类类型的方法：inspector.getTypeName()
    if (!inspector.getCategory().equals(ObjectInspector.Category.PRIMITIVE)) {
      throw new UDFArgumentException("input param Category must PRIMITIVE!");
    }

    // 校验函数入参的小类类型
    //  PRIMITIVE大类类型有     VOID, BOOLEAN, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, STRING,
    //  DATE, TIMESTAMP, BINARY, DECIMAL, VARCHAR, CHAR, INTERVAL_YEAR_MONTH,
    // INTERVAL_DAY_TIME,UNKNOWN 小类类型
    if (!inspector.getTypeName().equalsIgnoreCase(PrimitiveObjectInspector.PrimitiveCategory.INT.name())) {
      throw new UDFArgumentException("input param PRIMITIVE Category must INT type");
    }

    return new SumEvaluator();
  }

  /** 实现sum函数的核心类 */
  public static class SumEvaluator extends GenericUDAFEvaluator {

    /** 用于mapper端和reducer端的输出 */
    IntWritable output = new IntWritable();

    @Override
    public ObjectInspector init(Mode m, ObjectInspector[] parameters) throws HiveException {
      // 设置各个阶段的返回值类型

      // 因为 mapper阶段和reducer阶段都是输出int类型，那就不用分阶段了，这个比较特殊
      super.init(m, parameters);
      // writableIntObjectInspector(IntWritable)
      return PrimitiveObjectInspectorFactory.writableIntObjectInspector;
    }

    @Override
    public AggregationBuffer getNewAggregationBuffer() throws HiveException {
      // 获取新的bean对象
      return new SumAgg();
    }

    @Override
    public void reset(AggregationBuffer agg) throws HiveException {
      // bean对象可能会重复应用，应用前清0
      SumAgg sumAgg = (SumAgg) agg;
      sumAgg.setSum(0);
    }

    @Override
    public void iterate(AggregationBuffer agg, Object[] parameters) throws HiveException {
      // 把mapper中每行的数据汇总到一起，放到一个bean里面，这个bean在mapper和reducer的内部。
      // 通俗理解： 将 每行的 parameters里面的金额，汇总到 bean对象里

      Object object = parameters[0];
      int num = 0;
      if (object instanceof LazyInteger) {
        // LazyString ---> Text  --> String
        LazyInteger lz = (LazyInteger) object;
        IntWritable t = lz.getWritableObject();
        num = t.get();
      } else if (object instanceof IntWritable) {
        IntWritable t = (IntWritable) object;
        num = t.get();
      } else {
        num = (Integer) object;
      }

      SumAgg sumAgg = (SumAgg) agg;
      // 之前的 + 当前的 = 汇总后的
      sumAgg.setSum(sumAgg.getSum() + num);
    }

    @Override
    public Object terminatePartial(AggregationBuffer agg){
      // 把带有中间结果的bean转换成能实现序列化在mapper 和 reducer 端传输的对象。
      // 通俗理解： bean对象里的 汇总结果 序列化到 IntWritable， 用于输出到reducer
      SumAgg sumAgg = (SumAgg) agg;
      output.set(sumAgg.getSum());

      return output;
    }

    @Override
    public void merge(AggregationBuffer agg, Object partial) throws HiveException {
      // reduce端把mapper端输出的数据进行全局合并，把合并的结果放到bean里
      // 通俗理解： mapper 输出的局部数据 partial， 汇总到  bean对象里
      int num = 0;
      if (partial instanceof LazyInteger) {
        // LazyString ---> Text  --> String
        LazyInteger lz = (LazyInteger) partial;
        IntWritable t = lz.getWritableObject();
        num = t.get();
      } else if (partial instanceof IntWritable) {
        IntWritable t = (IntWritable) partial;
        num = t.get();
      } else {
        num = (Integer) partial;
      }

      SumAgg sumAgg = (SumAgg) agg;
      // 之前的 + 当前的 = 汇总后的
      sumAgg.setSum(sumAgg.getSum() + num);
    }

    @Override
    public Object terminate(AggregationBuffer agg) throws HiveException {
      // 把bean里的全局聚合结果，转换成能实现序列化的输出对象
      SumAgg sumAgg = (SumAgg) agg;
      output.set(sumAgg.getSum());

      return output;
    }

    /** 用来存mapper内部（局部） 或 reducer内部聚合的结果（全局） */
    public static class SumAgg extends AbstractAggregationBuffer {
      private int sum = 0;

      public int getSum() {
        return sum;
      }

      public void setSum(int sum) {
        this.sum = sum;
      }
    }
  }
}
