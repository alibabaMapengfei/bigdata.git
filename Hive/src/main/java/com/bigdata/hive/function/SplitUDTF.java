package com.bigdata.hive.function;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.lazy.LazyString;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;

/**
 * @Description（描述）: UDTF函数
 * @Version（版本）: 1.0
 * @Author（创建者）: ALIENWARE
 * @Date（创建时间）: Created in 2021/7/2.
 * @ * * * * * * * * * * * * * @
 */
public class SplitUDTF extends GenericUDTF{
	

	@Override
	public StructObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
		// 校验函数输入参数和 设置函数返回值类型
		// 1) 校验入参个数
		if(arguments.length != 1){
			throw new UDFArgumentException("input params must one");
		}
		
		ObjectInspector inspector = arguments[0];
		// 2) 校验参数的大类
		// 大类有： PRIMITIVE（基本类型）, LIST, MAP, STRUCT, UNION
		if(! inspector.getCategory().equals(ObjectInspector.Category.PRIMITIVE)){
			throw new UDFArgumentException("input params Category must PRIMITIVE");
		}
		
		// 3) 校验参数的小类
//			    VOID, BOOLEAN, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, STRING,
//			    DATE, TIMESTAMP, BINARY, DECIMAL, VARCHAR, CHAR, INTERVAL_YEAR_MONTH, INTERVAL_DAY_TIME,
//			    UNKNOWN
		if(! inspector.getTypeName().equalsIgnoreCase(PrimitiveObjectInspector.PrimitiveCategory.STRING.name())){
			throw new UDFArgumentException("input params PRIMITIVE Category type must STRING");
		}
		
		// 4) 设置函数返回值类型(struct<name:string, nickname:string>)
		  // struct内部字段的名称
	      List<String> names = new ArrayList<>();
	      names.add("name");
	      names.add("nickname");
	      
	      // struct内部字段的名称对应的类型
	      List<ObjectInspector> inspectors = new ArrayList<>();
	      inspectors.add(PrimitiveObjectInspectorFactory.writableStringObjectInspector);
	      inspectors.add(PrimitiveObjectInspectorFactory.writableStringObjectInspector);
	      
	      
		return ObjectInspectorFactory.getStandardStructObjectInspector(names, inspectors);
		
	}
	
	/**
	 * 函数输出类型
	 * 第一个参数：name
	 * 第二个参数：nickname
	 */
	Object[] outputs = new Object[]{new Text(), new Text()};

	@Override
	public void process(Object[] args) throws HiveException {
		// 核心方法 一行调用一次

		Object obj = args[0];
		
		String data = null;
		if(obj instanceof LazyString){
			LazyString lz = (LazyString)obj;
			Text t = lz.getWritableObject();
			data = t.toString();
		}else if(obj instanceof Text){
			Text t = (Text)obj;
			data = t.toString();
		}else{
			data = (String)obj;
		}
		
		// name1#n1;name2#n2
		String[] arr1 = data.split(";");
		// name1#n1
		for(String data2 : arr1){
			String[] arr2 = data2.split("#");
			String name = arr2[0];
			String nickname = arr2[1];

			// 想输出就调用forward()
			((Text)outputs[0]).set(name);
			((Text)outputs[1]).set(nickname);
			System.out.println("forward()");
			forward(outputs);
		}
	}

	@Override
	public void close() throws HiveException {

	}
}