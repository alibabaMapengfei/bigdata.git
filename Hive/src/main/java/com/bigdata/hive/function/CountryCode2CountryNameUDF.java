package com.bigdata.hive.function;

import org.apache.hadoop.hive.ql.exec.MapredContext;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.lazy.LazyString;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description（描述）: UDF函数
 * @Version（版本）: 1.0
 * @Author（创建者）: ALIENWARE
 * @Date（创建时间）: Created in 2021/7/2.
 * @ * * * * * * * * * * * * * @
 */
public class CountryCode2CountryNameUDF  extends GenericUDF {


	private static Map<String,String> countryMap = new HashMap<String,String>();

	static{
		// 将 字典文件数据写入内存
		try(
				InputStream is = CountryCode2CountryNameUDF.class.getResourceAsStream("/country_dict.txt");
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
		){
			String line = null;
			while((line = reader.readLine()) != null){
				String[] arr = line.split("\t");
				String code = arr[0];
				String name = arr[1];
				countryMap.put(code, name);
			}
			System.out.println("countryMap.size: " + countryMap.size());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void configure(MapredContext context) {
		JobConf jobConf = context.getJobConf();
		super.configure(context);
	}

	//初始化参数校验
	@Override
	public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
		System.out.println("initialize");
		// 校验函数输入参数和 设置函数返回值类型
		// 1) 校验入参个数
		if(arguments.length != 1){
			throw new UDFArgumentException("input params must one");
		}

		ObjectInspector inspector = arguments[0];
		// 2) 校验参数的大类
		// 大类有： PRIMITIVE（基本类型）, LIST, MAP, STRUCT, UNION
		if(! inspector.getCategory().equals(ObjectInspector.Category.PRIMITIVE)){
			throw new UDFArgumentException("input params Category must PRIMITIVE");
		}

		// 3) 校验参数的小类
//	    VOID, BOOLEAN, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, STRING,
//	    DATE, TIMESTAMP, BINARY, DECIMAL, VARCHAR, CHAR, INTERVAL_YEAR_MONTH, INTERVAL_DAY_TIME,
//	    UNKNOWN
		if(! inspector.getTypeName().equalsIgnoreCase(PrimitiveObjectInspector.PrimitiveCategory.STRING.name())){
			throw new UDFArgumentException("input params PRIMITIVE Category type must STRING");
		}

		// 4) 设置函数返回值类型
		// writableStringObjectInspector 里面有 Text， Text 里面有String类型
		return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
	}

	/**
	 * 定义函数输出对象
	 * 核心算法, 一行调用一次
	 */
	Text output = new Text();
	@Override
	public Object evaluate(DeferredObject[] arguments) throws HiveException {

		// 获取入参数据
		Object obj = arguments[0].get();

		String code = null;
		/*
		* LazyString
		*
		* */
		if(obj instanceof LazyString){
			LazyString lz = (LazyString)obj;
			Text t = lz.getWritableObject();
			code = t.toString();
		}else if(obj instanceof Text){
			Text t = (Text)obj;
			code = t.toString();
		}else{
			code = (String)obj;
		}
		// 翻译国家码
		String countryName = countryMap.get(code);
		countryName = countryName == null ? "某个小国" : countryName;

		output.set(countryName);
		return output;
	}

	//说明
	@Override
	public String getDisplayString(String[] strings) {

		return Arrays.toString(strings);
	}

}
