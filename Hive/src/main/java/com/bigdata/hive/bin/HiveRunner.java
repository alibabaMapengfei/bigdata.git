package com.bigdata.hive.bin;

import org.apache.hadoop.hive.cli.CliDriver;

public class HiveRunner {
    public static void main(String[] args) throws Exception {
        System.setProperty("jline.WindowsTerminal.directConsole", "false");
        int ret = new CliDriver().run(args);
        System.exit(ret);
    }
}
